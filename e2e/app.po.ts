import { browser, by, element, protractor } from 'protractor';
import { Locator } from 'protractor/built/locators';
import fs = require('fs');

export class AppPage {
  navigateTo() {
    return browser.get('/examples/angular2/');
  }

  getParagraphText() {
    return element(by.css('todo-app h1')).getText();
  }
  getTitle() {
    return browser.getTitle();
  }
  getInputText() {
    return element(by.css('input.new-todo')).getText();
  }
  getListItems() {
    return element.all(by.css('ul.todo-list li'));
  }
  isTodoCountPresent() {
    return  element(by.css('span.todo-count')).isPresent();
  }

  getTodoCount() {
    return element(by.css('span.todo-count')).getText();
  }
  createNewItem(itemName: string) {
    const inputField = element(by.css('input.new-todo'));
    inputField.sendKeys(itemName);
    inputField.sendKeys(protractor.Key.ENTER);
  }

   takeSnapshot() {
    browser.takeScreenshot().then(function (png) {
      const dirPath = './reports/screenshots/';
      if (!fs.existsSync('./reports')) {
          fs.mkdirSync('./reports');
          if (!fs.existsSync(dirPath)) {
            fs.mkdirSync(dirPath);
          }
      }
      const stream = fs.createWriteStream(dirPath + 'Failed_spec_screenshot.png');
      stream.write(new Buffer(png, 'base64'));
            stream.end();
      }, function (error) {
            console.log('failed to take screenshot');
  } );
 }
}
