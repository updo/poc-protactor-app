import { AppPage } from './app.po';

describe('poc-protactor-app App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display todos', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('todos');
  });
  it('should have  a title', () => {
    page.navigateTo();
    expect(page.getTitle()).toEqual('Angular2 • TodoMVC');
  });
  it('input newtodo should be empty', () => {
    page.navigateTo();
    expect(page.getInputText()).toBe('');
  });
/*   it('todocount shouldnt present', () => {
    page.navigateTo();
    expect(page.isTodoCountPresent()).toBe(false);
  }); */




});
