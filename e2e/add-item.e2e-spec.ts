import { AppPage } from './app.po';
describe('Adding new items', () => {

  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });


  it('should appear in the item list', async () => {
    const itemName = 'Write a guide1';
    page.navigateTo();
    page.createNewItem(itemName);
    expect(page.getListItems().last().getText()).toEqual(itemName);
  });

  it('todocount should be  present', async () => {
    const itemName = 'Write a guide2';
    page.navigateTo();
    page.createNewItem(itemName);
    expect(page.isTodoCountPresent()).toBeTruthy();
  });

  it('after creation one item left should appear', async () => {
    const itemName = 'Write a guide3';
    page.navigateTo();
    page.createNewItem(itemName);
    expect(page.getTodoCount()).toContain('items left');
  });
});
